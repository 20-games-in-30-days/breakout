extends CharacterBody2D

@export var start_speed := 800
@export var paddle_angle_factor := 10

var vel := Vector2.ZERO


func _ready():
	vel.y = -start_speed


func _physics_process(_delta):
	set_velocity(vel)
	set_up_direction(Vector2.UP)
	move_and_slide()
	var _v = vel
	
	if not break_brick():
		if is_on_wall():
			vel.x = -vel.x
			$Wall.play()
			
		if is_on_ceiling():
			vel.y = -vel.y
			$Wall.play()
		
		if is_on_floor():
			vel.y = -start_speed
			$Paddle.play()
			for slide in get_slide_collision_count():
				var collision = get_slide_collision(slide)
				# This should just be the paddle.
				vel.x = (transform.origin.x - collision.get_collider().position.x + randf_range(-0.1, 0.1)) * paddle_angle_factor 
	
	vel = (vel.normalized() * start_speed)


func break_brick() -> bool:
	for slide in get_slide_collision_count():
		var collision := get_slide_collision(slide)
		if collision.get_collider() is Brick:
			start_speed += (collision.get_collider().row / 4.0) * get_parent().speed_multiplier
			collision.get_collider().break_brick()
			
			if is_on_wall():
				vel.x = -vel.x
			
			if is_on_ceiling() or is_on_floor():
				vel.y = -vel.y

			$Wall.play()
			return true
	
	# if there were no collisions
	return false
